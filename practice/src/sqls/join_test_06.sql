/*
 * 请告诉我所有的员工（employee）的姓名及其管理者的姓名。所有的姓名都需要按照 `lastName, firstName`
 * 的格式输出，例如 'Bow, Anthony'。如果员工没有管理者，则其管理者的姓名输出为 '(Top Manager)'。
 * 输出需要包含如下信息：
 *
 * +───────────+──────────+
 * | employee  | manager  |
 * +───────────+──────────+
 *
 * 输出结果按照 `manager` 排序，然后按照 `employee` 排序。
 */
SELECT CONCAT(originalEmployees.lastName, ', ', originalEmployees.firstName) AS employee,
IFNULL (CONCAT(managers.lastName, ', ', managers.firstName), '(Top Manager)')AS manager
FROM employees originalEmployees
LEFT JOIN employees managers
ON originalEmployees.reportsTo = managers.employeeNumber
ORDER BY manager, employee;