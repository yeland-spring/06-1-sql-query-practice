/*
 * 请告诉我总金额大于 60000 的每一个订单（`order`）编号及其总金额。查询结果应当包含如下信息：
 * 
 * +──────────────+─────────────+
 * | orderNumber  | totalPrice  |
 * +──────────────+─────────────+
 *
 * 其结果应当以 `orderNumber` 排序。
 */
SELECT orderdetails.orderNumber AS orderNumber,
SUM (quantityOrdered * priceEach) AS totalPrice
FROM `orders`
LEFT JOIN `orderdetails`
ON orders.orderNumber = orderdetails.orderNumber
GROUP BY orderNumber
HAVING totalPrice > 60000;